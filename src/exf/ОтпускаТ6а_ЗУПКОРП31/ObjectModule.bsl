﻿
Функция СведенияОВнешнейОбработке() Экспорт
	
    ПараметрыРегистрации = Новый Структура;
	// Строка, вид обработки, один из возможных: «ДополнительнаяОбработка», «ДополнительныйОтчет», «ЗаполнениеОбъекта», «Отчет», «ПечатнаяФорма» или «СозданиеСвязанныхОбъектов»
    ПараметрыРегистрации.Вставить("Вид", "ПечатнаяФорма");
	// Массив строк имен объектов метаданных в формате: <ИмяКлассаОбъектаМетаданного>.[ * | <ИмяОбъектаМетаданных>]. Например, «Документ.СчетЗаказ» или «Справочник.*»
	Назначения = Новый Массив;
	Назначения.Добавить("Документ.ОтпускаСотрудников");
    ПараметрыРегистрации.Вставить("Назначение", Назначения);
	// Наименование обработки, которым будет заполнено наименование элемента справочника, краткое описание для идентификации обработки администратором
    ПараметрыРегистрации.Вставить("Наименование", "Приказ Т-6а (Невада)");
	// Версия обработки в формате <старший номер>.<младший номер> используется при загрузке обработок в информационную базу. Например, 1.0
    ПараметрыРегистрации.Вставить("Версия", "1.0");
    ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);
	// Краткая информация по обработке, описание обработки
    ПараметрыРегистрации.Вставить("Информация", "Приказ Т-6а (Невада)");
    ПараметрыРегистрации.Вставить("ВерсияБСП", "2.3.1.83");
	Команды = ТаблицаКоманд();
	ДобавитьКоманду(Команды,
	                "Приказ Т-6а (Невада)",
	                "ПечатьПриказ",
	                ДополнительныеОтчетыИОбработкиКлиентСервер.ТипКомандыВызовСерверногоМетода(),
	                Ложь);
	ПараметрыРегистрации.Вставить("Команды", Команды);
	
    Возврат ПараметрыРегистрации;
	
КонецФункции

Функция ТаблицаКоманд()
	
    Команды = Новый ТаблицаЗначений;
    Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
    Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("ЗаменяемыеКоманды", Новый ОписаниеТипов("Строка"));
    Возврат Команды;
	
КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "", ЗаменяемыеКоманды = "")
	
    НоваяКоманда = ТаблицаКоманд.Добавить();
    НоваяКоманда.Представление = Представление;
    НоваяКоманда.Идентификатор = Идентификатор;
    НоваяКоманда.Использование = Использование;
    НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
    НоваяКоманда.Модификатор = Модификатор;
	НоваяКоманда.ЗаменяемыеКоманды = ЗаменяемыеКоманды;
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
    ПечатнаяФорма = УправлениеПечатью.СведенияОПечатнойФорме(КоллекцияПечатныхФорм, "ПечатьПриказ");
    Если ПечатнаяФорма <> Неопределено Тогда
		ПечатнаяФорма.ТабличныйДокумент = СформироватьПечФорму(МассивОбъектов, ОбъектыПечати);	
        ПечатнаяФорма.СинонимМакета = НСтр("ru = 'Приказ Т-6а (Невада)'");
	КонецЕсли;
	
КонецПроцедуры

Функция СформироватьПечФорму(МассивОбъектов, ОбъектыПечати)
	
	НастройкиПечатныхФорм = ЗарплатаКадрыПовтИсп.НастройкиПечатныхФорм();
	
	ДокументРезультат = Новый ТабличныйДокумент;
	ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ДокументРезультат.АвтоМасштаб = Истина;
	
	НомерСтрокиНачало = ДокументРезультат.ВысотаТаблицы + 1;
	
	Макет = ПолучитьМакет("ПФ_MXL_Т6а");
	
	ДокументРезультат.КлючПараметровПечати = "ПараметрыПечати_ПриказОПредоставленииОтпуска";
	
	ДанныеЗаполнения = ДанныеДляПечатиКадровогоПриказаТ6а(МассивОбъектов);
	
	СсылкаНаОбъект = Неопределено;
	
	Если Макет <> Неопределено Тогда
		
		ПервыйПриказ = Истина;
		Для каждого ПараметрыМакетаКлючИЗначение Из ДанныеЗаполнения Цикл
			
			НомерСтрокиНачало = ДокументРезультат.ВысотаТаблицы + 1; 
			
			ПараметрыМакета = ПараметрыМакетаКлючИЗначение;
			
			Если Не ПервыйПриказ Тогда
				ДокументРезультат.ВывестиГоризонтальныйРазделительСтраниц();
			Иначе
				ПервыйПриказ = Ложь;
			КонецЕсли;
			
			МакетШапки = Макет.ПолучитьОбласть("Шапка");
			МакетШапки.Параметры.Заполнить(ПараметрыМакета);
			МакетШапки.Параметры.Номер = КадровыйУчетРасширенный.НомерКадровогоПриказа(ПараметрыМакета.Номер);
			ДокументРезультат.Вывести(МакетШапки);
			
			МакетПодвала = Макет.ПолучитьОбласть("Подвал");
			МакетПодвала.Параметры.Заполнить(ПараметрыМакета);
			
			Для каждого СтрокаОтпуска Из ПараметрыМакета.ДанныеОтпусков Цикл
				
				МакетСтроки = Макет.ПолучитьОбласть("Строка");
				МакетСтроки.Параметры.Заполнить(СтрокаОтпуска);
				
				РезультатСклонения = "";
				Если ФизическиеЛицаЗарплатаКадры.Просклонять(Строка(СтрокаОтпуска.ФИОПолные), 3, РезультатСклонения, СтрокаОтпуска.Пол) Тогда
					МакетСтроки.Параметры.ФИОПолные	= РезультатСклонения
				КонецЕсли;
				
				МакетСтроки.Параметры.ТабельныйНомер = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(СтрокаОтпуска.ТабельныйНомер, Истина, Истина);
				
				ОписаниеОтпуска = Строка(СтрокаОтпуска.ПолноеНаименованиеОтпуска) + " (" + СтрокаОтпуска.КоличествоДней + ")";
				
				Если СтрокаОтпуска.ОсновнойОтпуск Тогда
					МакетСтроки.Параметры.ОсновнойОтпуск = ОписаниеОтпуска;
					МакетСтроки.Параметры.ДополнительныйОтпуск = "";
				Иначе
					МакетСтроки.Параметры.ОсновнойОтпуск = "";
					МакетСтроки.Параметры.ДополнительныйОтпуск = ОписаниеОтпуска;
				КонецЕсли;
		
				Если НастройкиПечатныхФорм.ВыводитьПолнуюИерархиюПодразделений И ЗначениеЗаполнено(МакетСтроки.Параметры.Подразделение) Тогда
					МакетСтроки.Параметры.Подразделение = МакетСтроки.Параметры.Подразделение.ПолноеНаименование();
				КонецЕсли;
				
				МассивМакетов = Новый Массив;
				МассивМакетов.Добавить(МакетСтроки);
				МассивМакетов.Добавить(МакетПодвала);
				
				Если НЕ ДокументРезультат.ПроверитьВывод(МассивМакетов) Тогда
					
					ДокументРезультат.ВывестиГоризонтальныйРазделительСтраниц();
					
					МакетШапкиТаблицы = Макет.ПолучитьОбласть("ПовторятьПриПечати");
					ДокументРезультат.Вывести(МакетШапкиТаблицы);
					
				КонецЕсли; 
				
				ДокументРезультат.Вывести(МакетСтроки);
				
			КонецЦикла;
			
			ДокументРезультат.Вывести(МакетПодвала);
			
			УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ДокументРезультат, НомерСтрокиНачало, ОбъектыПечати, ПараметрыМакета.Ссылка);
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат ДокументРезультат;
	
КонецФункции

Функция ДанныеДляПечатиКадровогоПриказаТ6а(МассивОбъектов)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудниковСотрудники.Сотрудник,
	|	ОтпускаСотрудниковСотрудники.ВидОтпуска,
	|	ОтпускаСотрудниковСотрудники.ДатаНачала,
	|	ОтпускаСотрудниковСотрудники.ДатаОкончания,
	|	ОтпускаСотрудниковСотрудники.НачалоПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	ОтпускаСотрудниковСотрудники.КонецПериодаЗаКоторыйПредоставляетсяОтпуск,
	|	ОтпускаСотрудниковСотрудники.Основание,
	|	ОтпускаСотрудниковСотрудники.КоличествоДней,
	|	ОтпускаСотрудниковСотрудники.НомерСтроки
	|ПОМЕСТИТЬ ВТДанныеОтпусков
	|ИЗ
	|	Документ.ОтпускаСотрудников КАК ОтпускаСотрудников
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОтпускаСотрудников.Сотрудники КАК ОтпускаСотрудниковСотрудники
	|		ПО ОтпускаСотрудников.Ссылка = ОтпускаСотрудниковСотрудники.Ссылка
	|ГДЕ
	|	ОтпускаСотрудников.Ссылка В(&МассивОбъектов)
	|	И ОтпускаСотрудниковСотрудники.КоличествоДней > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудниковСотрудники.Сотрудник,
	|	ОтпускаСотрудников.ВидОтпуска,
	|	ОтпускаСотрудниковСотрудники.ДатаНачала,
	|	ОтпускаСотрудниковСотрудники.ДатаОкончания,
	|	ДАТАВРЕМЯ(1, 1, 1),
	|	ДАТАВРЕМЯ(1, 1, 1),
	|	ОтпускаСотрудниковСотрудники.Основание,
	|	РАЗНОСТЬДАТ(ОтпускаСотрудниковСотрудники.ДатаНачала, ОтпускаСотрудниковСотрудники.ДатаОкончания, ДЕНЬ) + 1,
	|	ОтпускаСотрудниковСотрудники.НомерСтроки
	|ИЗ
	|	Документ.ОтпускБезСохраненияОплатыСписком КАК ОтпускаСотрудников
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОтпускБезСохраненияОплатыСписком.Сотрудники КАК ОтпускаСотрудниковСотрудники
	|		ПО ОтпускаСотрудников.Ссылка = ОтпускаСотрудниковСотрудники.Ссылка
	|ГДЕ
	|	ОтпускаСотрудников.Ссылка В(&МассивОбъектов)
	|	И РАЗНОСТЬДАТ(ОтпускаСотрудниковСотрудники.ДатаНачала, ОтпускаСотрудниковСотрудники.ДатаОкончания, ДЕНЬ) + 1 > 0
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудников.Дата,
	|	ОтпускаСотрудников.Номер,
	|	ОтпускаСотрудников.Организация,
	|	ОтпускаСотрудников.Руководитель,
	|	ОтпускаСотрудников.ДолжностьРуководителя,
	|	МИНИМУМ(ДанныеОтпусков.ДатаНачала) КАК Период
	|ПОМЕСТИТЬ ВТДанныеШапкиДокументов
	|ИЗ
	|	Документ.ОтпускаСотрудников КАК ОтпускаСотрудников
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТДанныеОтпусков КАК ДанныеОтпусков
	|		ПО ДанныеОтпусков.Ссылка = ОтпускаСотрудников.Ссылка
	|ГДЕ
	|	ОтпускаСотрудников.Ссылка В(&МассивОбъектов)
	|
	|СГРУППИРОВАТЬ ПО
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудников.Дата,
	|	ОтпускаСотрудников.Номер,
	|	ОтпускаСотрудников.Организация,
	|	ОтпускаСотрудников.Руководитель,
	|	ОтпускаСотрудников.ДолжностьРуководителя
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудников.Дата,
	|	ОтпускаСотрудников.Номер,
	|	ОтпускаСотрудников.Организация,
	|	ОтпускаСотрудников.Руководитель,
	|	ОтпускаСотрудников.ДолжностьРуководителя,
	|	МИНИМУМ(ДанныеОтпусков.ДатаНачала)
	|ИЗ
	|	Документ.ОтпускБезСохраненияОплатыСписком КАК ОтпускаСотрудников
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТДанныеОтпусков КАК ДанныеОтпусков 
	|		ПО ДанныеОтпусков.Ссылка = ОтпускаСотрудников.Ссылка
	|ГДЕ
	|	ОтпускаСотрудников.Ссылка В(&МассивОбъектов)
	|
	|СГРУППИРОВАТЬ ПО
	|	ОтпускаСотрудников.Ссылка,
	|	ОтпускаСотрудников.Дата,
	|	ОтпускаСотрудников.Номер,
	|	ОтпускаСотрудников.Организация,
	|	ОтпускаСотрудников.Руководитель,
	|	ОтпускаСотрудников.ДолжностьРуководителя";
	
	Запрос.Выполнить();
	
	ОписательВременныхТаблиц = КадровыйУчет.ОписательВременныхТаблицДляСоздатьВТКадровыеДанныеСотрудников(
		Запрос.МенеджерВременныхТаблиц, "ВТДанныеОтпусков", "Сотрудник,ДатаНачала");
	КадровыйУчет.СоздатьВТКадровыеДанныеСотрудников(ОписательВременныхТаблиц, Истина, "ФИОПолные,ТабельныйНомер,Подразделение,Должность,Пол");
	
	ЗарплатаКадры.СоздатьВТФИООтветственныхЛиц(Запрос.МенеджерВременныхТаблиц, Истина, "Руководитель", "ВТДанныеШапкиДокументов");
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ДанныеШапкиДокументов.Ссылка,
		|	ДанныеШапкиДокументов.Организация.НаименованиеПолное КАК ОрганизацияНаименованиеПолное,
		|	ДанныеШапкиДокументов.Организация.КодПоОКПО КАК ОрганизацияКодПоОКПО,
		|	ДанныеШапкиДокументов.Дата,
		|	ДанныеШапкиДокументов.Номер,
		|	ДанныеШапкиДокументов.ДолжностьРуководителя,
		|	ФИООтветственныхЛиц.РасшифровкаПодписи КАК ФИОРуководителя
		|ИЗ
		|	ВТДанныеШапкиДокументов КАК ДанныеШапкиДокументов
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТФИООтветственныхЛиц КАК ФИООтветственныхЛиц
		|		ПО ДанныеШапкиДокументов.Ссылка = ФИООтветственныхЛиц.Ссылка
		|			И ДанныеШапкиДокументов.Руководитель = ФИООтветственныхЛиц.ФизическоеЛицо
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДанныеОтпусков.Ссылка КАК Ссылка,
		|	ДанныеОтпусков.НомерСтроки,
		|	ДанныеОтпусков.Сотрудник КАК Сотрудник,
		|	КадровыеДанныеСотрудников.ФИОПолные КАК ФИОПолные,
		|	КадровыеДанныеСотрудников.ТабельныйНомер,
		|	КадровыеДанныеСотрудников.Пол,
		|	КадровыеДанныеСотрудников.Подразделение,
		|	КадровыеДанныеСотрудников.Должность,
		|	ДанныеОтпусков.ВидОтпуска,
		|	ВЫБОР
		|		КОГДА ДанныеОтпусков.ВидОтпуска = ЗНАЧЕНИЕ(Справочник.ВидыОтпусков.Основной)
		|			ТОГДА ИСТИНА
		|		ИНАЧЕ ЛОЖЬ
		|	КОНЕЦ КАК ОсновнойОтпуск,
		|	ВЫБОР
		|		КОГДА ВидыОтпусков.НаименованиеПолное = """"
		|			ТОГДА ВидыОтпусков.Наименование
		|		ИНАЧЕ ВидыОтпусков.НаименованиеПолное
		|	КОНЕЦ КАК ПолноеНаименованиеОтпуска,
		|	ДанныеОтпусков.ДатаНачала,
		|	ДанныеОтпусков.ДатаОкончания,
		|	ДанныеОтпусков.НачалоПериодаЗаКоторыйПредоставляетсяОтпуск,
		|	ДанныеОтпусков.КонецПериодаЗаКоторыйПредоставляетсяОтпуск,
		|	ДанныеОтпусков.Основание,
		|	ДанныеОтпусков.КоличествоДней
		|ИЗ
		|	ВТДанныеОтпусков КАК ДанныеОтпусков
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТКадровыеДанныеСотрудников КАК КадровыеДанныеСотрудников
		|		ПО ДанныеОтпусков.ДатаНачала = КадровыеДанныеСотрудников.Период
		|			И ДанныеОтпусков.Сотрудник = КадровыеДанныеСотрудников.Сотрудник
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ВидыОтпусков КАК ВидыОтпусков
		|		ПО ДанныеОтпусков.ВидОтпуска = ВидыОтпусков.Ссылка
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка,
		|	ДанныеОтпусков.НомерСтроки,
		|	Сотрудник";
		
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	ТаблицаОтпусков = РезультатЗапроса[1].Выгрузить();
	
	ТаблОП = ПолучитьОП(МассивОбъектов);
	
	тзУполГруппировка = ТаблОП.Скопировать();
    тзУполГруппировка.Свернуть("Ссылка,СтруктурнаяЕдиница,ФИО,Должность,НашлиУЛ");
	
	ДанныеПечатныхФорм = Новый Массив;
	
	ВыборкаПоРегистраторам = РезультатЗапроса[0].Выбрать();
	Пока ВыборкаПоРегистраторам.Следующий() Цикл
		
		СтрокиПоДок = тзУполГруппировка.НайтиСтроки(Новый Структура("Ссылка", ВыборкаПоРегистраторам.Ссылка));
		Для Каждого Стр Из СтрокиПоДок Цикл		
			ДанныеДокумента = Новый Структура("Ссылка,Номер,Дата,ОрганизацияНаименованиеПолное,ОрганизацияКодПоОКПО,ДолжностьРуководителя,ФИОРуководителя");
			ЗаполнитьЗначенияСвойств(ДанныеДокумента, ВыборкаПоРегистраторам);
			
			Если Стр.НашлиУЛ Тогда
				ДанныеДокумента.ДолжностьРуководителя = Стр.Должность;	
				ДанныеДокумента.ФИОРуководителя = Стр.ФИО;
				ДанныеДокумента.ОрганизацияНаименованиеПолное = ДанныеДокумента.ОрганизацияНаименованиеПолное + Символы.ПС + Стр.СтруктурнаяЕдиница;
			КонецЕсли;
			
			ДанныеОтпусков = ТаблицаОтпусков.СкопироватьКолонки();
			СтрокиПоОП = ТаблОП.Скопировать(Новый Структура("Ссылка,СтруктурнаяЕдиница,НашлиУЛ", Стр.Ссылка, Стр.СтруктурнаяЕдиница, Стр.НашлиУЛ)).ВыгрузитьКолонку("НомерСтроки");
			Для Каждого СтрОтпуск Из ТаблицаОтпусков Цикл
				Если СтрОтпуск.Ссылка = ВыборкаПоРегистраторам.Ссылка И СтрокиПоОП.Найти(СтрОтпуск.НомерСтроки) <> Неопределено Тогда
					НоваяСтр = ДанныеОтпусков.Добавить();
					ЗаполнитьЗначенияСвойств(НоваяСтр, СтрОтпуск);
				КонецЕсли;
			КонецЦикла;
			ДанныеДокумента.Вставить("ДанныеОтпусков", ДанныеОтпусков);
			
			ДанныеПечатныхФорм.Добавить(ДанныеДокумента);
		КонецЦикла;
		
	КонецЦикла; 
	
	Возврат ДанныеПечатныхФорм;
	
КонецФункции

Функция ПолучитьОП(МассивОбъектов)
	
	Заголовки = Новый Массив;
	Заголовки.Добавить("Приказ");
	Заголовки.Добавить("ОП");
			
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ОтпускаСотрудниковСотрудники.Ссылка,
	|	ОтпускаСотрудниковСотрудники.НомерСтроки,
	|	Отпуск.Ссылка КАК Ссылка1,
	|	ОтпускДополнительныеРеквизиты.Свойство.Заголовок КАК Заголовок,
	|	ОтпускДополнительныеРеквизиты.Значение
	|ИЗ
	|	Документ.ОтпускаСотрудников.Сотрудники КАК ОтпускаСотрудниковСотрудники
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.Отпуск КАК Отпуск
	|			ЛЕВОЕ СОЕДИНЕНИЕ Документ.Отпуск.ДополнительныеРеквизиты КАК ОтпускДополнительныеРеквизиты
	|				ЛЕВОЕ СОЕДИНЕНИЕ ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения КАК ДополнительныеРеквизитыИСведения
	|				ПО ОтпускДополнительныеРеквизиты.Свойство = ДополнительныеРеквизитыИСведения.Ссылка
	|					И (ОтпускДополнительныеРеквизиты.Свойство.Заголовок В (&Заголовки))
	|			ПО Отпуск.Ссылка = ОтпускДополнительныеРеквизиты.Ссылка
	|		ПО ОтпускаСотрудниковСотрудники.Сотрудник = Отпуск.Сотрудник
	|ГДЕ
	|	ОтпускаСотрудниковСотрудники.Ссылка В(&Ссылка)";
					
	Запрос.УстановитьПараметр("Ссылка", МассивОбъектов);
	Запрос.УстановитьПараметр("Заголовки", Заголовки);
					
	тзУполн = Запрос.Выполнить().Выгрузить();
	тзСотрудники = тзУполн.Скопировать();
	тзСотрудники.Свернуть("Ссылка, НомерСтроки");
	
	тзУполномоченные = Новый ТаблицаЗначений;
	тзУполномоченные.Колонки.Добавить("Ссылка");
	тзУполномоченные.Колонки.Добавить("НомерСтроки");
	тзУполномоченные.Колонки.Добавить("СтруктурнаяЕдиница");
	тзУполномоченные.Колонки.Добавить("ФИО");
	тзУполномоченные.Колонки.Добавить("Должность");
	тзУполномоченные.Колонки.Добавить("НашлиУЛ");
	
	Для каждого СтрДок Из тзСотрудники Цикл
		СтрокиПоДокСотр = тзУполн.Скопировать(Новый Структура("Ссылка, НомерСтроки", СтрДок.Ссылка, СтрДок.НомерСтроки));
		
		печНаименованиеОП = "";
		печФИО = "";
		печДолжность = "";
			
		НоваяСтр = тзУполномоченные.Добавить();
		НоваяСтр.НомерСтроки = СтрДок.НомерСтроки;
		НоваяСтр.Ссылка = СтрДок.Ссылка;
			
		Приказ_Уп = СтрокиПоДокСотр.Найти("Приказ");
		ОП = СтрокиПоДокСотр.Найти("ОП");
		Если (ОП <> Неопределено И ЗначениеЗаполнено(ОП.Значение)) Тогда 
			Приказ = ?(Приказ_Уп <> Неопределено И ЗначениеЗаполнено(Приказ_Уп.Значение), Приказ_Уп.Значение, "Устава");
			НоваяСтр.ФИО 				= ФизическиеЛицаЗарплатаКадрыКлиентСервер.ФамилияИнициалы(СтрДок.Ссылка.Руководитель.Наименование);
			НоваяСтр.Должность 			= Строка(СтрДок.Ссылка.ДолжностьРуководителя) + " на основании " + Приказ;
			НоваяСтр.СтруктурнаяЕдиница = ОП.Значение;
			НоваяСтр.НашлиУЛ 			= Истина;
		Иначе //берем из организации	
			НоваяСтр.НашлиУЛ 			= Ложь;
		КонецЕсли;
	КонецЦикла;
			
	Возврат тзУполномоченные;
	
КонецФункции
