﻿
Функция СведенияОВнешнейОбработке() Экспорт
	
    ПараметрыРегистрации = Новый Структура;
	// Строка, вид обработки, один из возможных: «ДополнительнаяОбработка», «ДополнительныйОтчет», «ЗаполнениеОбъекта», «Отчет», «ПечатнаяФорма» или «СозданиеСвязанныхОбъектов»
    ПараметрыРегистрации.Вставить("Вид", "ПечатнаяФорма");
	// Массив строк имен объектов метаданных в формате: <ИмяКлассаОбъектаМетаданного>.[ * | <ИмяОбъектаМетаданных>]. Например, «Документ.СчетЗаказ» или «Справочник.*»
	Назначения = Новый Массив;
	Назначения.Добавить("Справочник.Сотрудники");
    ПараметрыРегистрации.Вставить("Назначение", Назначения);
	// Наименование обработки, которым будет заполнено наименование элемента справочника, краткое описание для идентификации обработки администратором
    ПараметрыРегистрации.Вставить("Наименование", "Заявление на перечисление ден. ср-в на р/с (Невада)");
	// Версия обработки в формате <старший номер>.<младший номер> используется при загрузке обработок в информационную базу. Например, 1.0
    ПараметрыРегистрации.Вставить("Версия", "1.0");
    ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);
	// Краткая информация по обработке, описание обработки
    ПараметрыРегистрации.Вставить("Информация", "Заявление на перечисление ден. ср-в на р/с (Невада)");
    ПараметрыРегистрации.Вставить("ВерсияБСП", "2.3.1.83");
	Команды = ТаблицаКоманд();
	ДобавитьКоманду(Команды,
	                "Заявление на перечисление ден. ср-в на р/с (Невада)",
	                "ПечатьЗаявление",
	                ДополнительныеОтчетыИОбработкиКлиентСервер.ТипКомандыВызовСерверногоМетода(),
	                Ложь);
	ПараметрыРегистрации.Вставить("Команды", Команды);
	
    Возврат ПараметрыРегистрации;
	
КонецФункции

Функция ТаблицаКоманд()
	
    Команды = Новый ТаблицаЗначений;
    Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
    Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
    Команды.Колонки.Добавить("ЗаменяемыеКоманды", Новый ОписаниеТипов("Строка"));
    Возврат Команды;
	
КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "", ЗаменяемыеКоманды = "")
	
    НоваяКоманда = ТаблицаКоманд.Добавить();
    НоваяКоманда.Представление = Представление;
    НоваяКоманда.Идентификатор = Идентификатор;
    НоваяКоманда.Использование = Использование;
    НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
    НоваяКоманда.Модификатор = Модификатор;
	НоваяКоманда.ЗаменяемыеКоманды = ЗаменяемыеКоманды;
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
    ПечатнаяФорма = УправлениеПечатью.СведенияОПечатнойФорме(КоллекцияПечатныхФорм, "ПечатьЗаявление");
    Если ПечатнаяФорма <> Неопределено Тогда
		ПечатнаяФорма.ТабличныйДокумент = СформироватьПечФорму(МассивОбъектов, ОбъектыПечати);	
        ПечатнаяФорма.СинонимМакета = НСтр("ru = 'Заявление на перечисление ден. ср-в на р/с (Невада)'");
	КонецЕсли;
	
КонецПроцедуры

Функция СформироватьПечФорму(МассивОбъектов, ОбъектыПечати)
	
	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.КлючПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЗаявлениеНаПеречислениеДСНаРС_Печать";
	
	ДанныеДоговоров = ДанныеТрудовыхДоговоров(МассивОбъектов, Ложь, Ложь);
	Для каждого ДанныеДоговора Из ДанныеДоговоров Цикл
		
		НомерСтрокиНачало = ТабДокумент.ВысотаТаблицы + 1;
		
		Если НомерСтрокиНачало > 1 Тогда
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		ТаблицаТрудовогоДоговора = Новый ТабличныйДокумент;
		
		ПараметрыНумерации = Новый Структура;
		УстановитьНомерРазделаВПараметрахНумерации(ПараметрыНумерации, 1);
		УстановитьНомерПунктаВПараметрахНумерации(ПараметрыНумерации, 1);
		
		// запоминаем области макета
		Макет = ПолучитьМакет("Заявление");
		Макет.Параметры.Заполнить(ДанныеДоговора);
		
		Макет.Параметры.ДатаПриема = Формат(ДанныеДоговора.ДатаПриема,"ДФ=dd.MM.yyyy");  
		
		РезультатСклонения = "";
		Если ФизическиеЛицаЗарплатаКадры.Просклонять(ДанныеДоговора.СотрудникФИОПолные, 2, РезультатСклонения, ДанныеДоговора.ПолРаботника) Тогда
			Макет.Параметры.ФИО = РезультатСклонения;
		КонецЕсли;
		
		Макет.Параметры.БИК = ?(ЗначениеЗаполнено(Макет.Параметры.Банк), ", " + ДанныеДоговора.БИК, ДанныеДоговора.БИК);
		
		//фио ответственного либо уполномоченного необходимо просклонять		
		Фамилия = ДанныеДоговора.ФамилияРук;
		Имя = ?(ЗначениеЗаполнено(ДанныеДоговора.ИмяРук), Лев(ДанныеДоговора.ИмяРук, 1) + ".", "");
		Отчество = ?(ЗначениеЗаполнено(ДанныеДоговора.ОтчествоРук), Лев(ДанныеДоговора.ОтчествоРук, 1) + ".", "");
		ИнициалыРук = Имя + Отчество;
			
		РезультатСклонения = "";
		Если ФизическиеЛицаЗарплатаКадры.Просклонять(ДанныеДоговора.ФамилияРук, 3, РезультатСклонения, ДанныеДоговора.ПолРук) Тогда
			Фамилия = РезультатСклонения;
		КонецЕсли;
		Макет.Параметры.ФИОРук = ИнициалыРук + Фамилия;
					
		// выводим данные
		ТабДокумент.Вывести(Макет);
		
		ТабДокумент.Вывести(ТаблицаТрудовогоДоговора);
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабДокумент, НомерСтрокиНачало, ОбъектыПечати, ДанныеДоговора.Ссылка);
		
	КонецЦикла;
	
    Возврат ТабДокумент;
	
КонецФункции

Функция ДанныеТрудовыхДоговоров(МассивОбъектов, ДистанционныеРаботы, ДоговорСНадомником) 
	
	ДанныеТрудовыхДоговоров = Новый Массив;	
	
	ТаблицаКадровыхДанных = КадровыйУчет.КадровыеДанныеСотрудников(Ложь, МассивОбъектов, КадровыеДанныеДляТрудовогоДоговора(), ТекущаяДатаСеанса());
	
	Для Каждого ТекДанные Из ТаблицаКадровыхДанных Цикл 
		
		ДанныеДоговора = ПараметрыТрудовогоДоговора();
		
		АдресаОрганизаций = УправлениеКонтактнойИнформациейЗарплатаКадры.АдресаОрганизаций(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ТекДанные.Организация));
		
		Сведения = Новый СписокЗначений;
		Сведения.Добавить("", "НаимЮЛПол");
		Сведения.Добавить("", "ФИОРук"); 
		Сведения.Добавить("", "ДолжнРук");
		Сведения.Добавить("", "ПолРук");
		Сведения.Добавить("", "ФамилияРук");
		Сведения.Добавить("", "ИмяРук");
		Сведения.Добавить("", "ОтчествоРук");
		
		УстановитьПривилегированныйРежим(Истина);
		ОргСведения = РегламентированнаяОтчетностьВызовСервера.ПолучитьСведенияОбОрганизации(ТекДанные.Организация, ТекущаяДатаСеанса(), Сведения);
		УстановитьПривилегированныйРежим(Ложь);
			
		Если ОргСведения.Свойство("НаимЮЛПол") Тогда
			ДанныеДоговора.ОрганизацияНаименованиеПолное = ОргСведения.НаимЮЛПол;
		КонецЕсли;
			
		Если ЗначениеЗаполнено(ОргСведения.ФамилияРук) Тогда
			ДанныеДоговора.ФамилияРук = ОргСведения.ФамилияРук;			
		КонецЕсли;
		Если ЗначениеЗаполнено(ОргСведения.ИмяРук) Тогда
			ДанныеДоговора.ИмяРук = ОргСведения.ИмяРук;			
		КонецЕсли;
		Если ЗначениеЗаполнено(ОргСведения.ОтчествоРук) Тогда
			ДанныеДоговора.ОтчествоРук = ОргСведения.ОтчествоРук;			
		КонецЕсли;
				
		ИсключаемыеИЗЗаполненияКолонки ="";                           
		ЗаполнитьЗначенияСвойств(ДанныеДоговора, ТекДанные, , ИсключаемыеИЗЗаполненияКолонки);
		
		ДанныеДоговора.СотрудникФИОПолные =	ТекДанные.ФИОПолные;
			
		Если ЗначениеЗаполнено(ТекДанные.ЗарплатныйПроект.Банк) Тогда
			ДанныеДоговора.Банк = ТекДанные.ЗарплатныйПроект.Банк;	
			ДанныеДоговора.БИК = ТекДанные.ЗарплатныйПроект.Банк.Код;
		КонецЕсли;
			
		ДанныеТрудовыхДоговоров.Добавить(ДанныеДоговора);

	КонецЦикла;
		
	Возврат ДанныеТрудовыхДоговоров;
	
КонецФункции

Функция ПараметрыТрудовогоДоговора(ДистанционныеРаботы = Ложь, ДоговорСНадомником = Ложь)
	
	ПараметрыДоговора = Новый Структура;
	
	ПараметрыДоговора.Вставить("Ссылка");
	
	ПараметрыДоговора.Вставить("СотрудникФИОПолные");
	ПараметрыДоговора.Вставить("ФамилияИО");  
	ПараметрыДоговора.Вставить("ПолРаботника"); 
	ПараметрыДоговора.Вставить("НомерЛицевогоСчета"); 
	ПараметрыДоговора.Вставить("Банк");
	ПараметрыДоговора.Вставить("БИК");
	
	ПараметрыДоговора.Вставить("ДатаПриема");
	ПараметрыДоговора.Вставить("Организация");  
	
	ПараметрыДоговора.Вставить("ОрганизацияНаименованиеПолное");
	ПараметрыДоговора.Вставить("ОрганизацияНаименованиеСокращенное");
	
	ПараметрыДоговора.Вставить("РуководительФИОПолные");
	ПараметрыДоговора.Вставить("ПолРук"); 
	ПараметрыДоговора.Вставить("ИмяРук");   
	ПараметрыДоговора.Вставить("ФамилияРук");
	ПараметрыДоговора.Вставить("ОтчествоРук");
	ПараметрыДоговора.Вставить("ФРуководителя");
	ПараметрыДоговора.Вставить("РуководительДолжность");
	
	Возврат ПараметрыДоговора;
	
КонецФункции

Процедура УстановитьНомерРазделаВПараметрахНумерации(ПараметрыНумерации, Знач НомерРаздела)
	
	ПараметрыНумерации.Вставить("НомерРаздела", НомерРаздела);
	ПараметрыНумерации.Вставить("НомерРазделаВРимскойНотации",
		СтроковыеФункцииКлиентСервер.ПреобразоватьЧислоВРимскуюНотацию(НомерРаздела, Ложь));
	
КонецПроцедуры

Процедура УстановитьНомерПунктаВПараметрахНумерации(ПараметрыНумерации, Знач НомерПункта)
	
	ПараметрыНумерации.Вставить("НомерПункта", НомерПункта);
	
КонецПроцедуры

Функция КадровыеДанныеДляТрудовогоДоговора() Экспорт
	
	КадровыеДанные =
		"ГоловнаяОрганизация,
		|Организация,
		|Подразделение,
		|Должность,
		|ТекущееПодразделение,
		|ТекущаяДолжность,
		|КоличествоСтавок,
		|ВидЗанятости,
		|ФИОПолные,
		|ФамилияИО,
		|АдресМестаПроживанияПредставление,
		|АдресПоПропискеПредставление,
		|ДокументПредставление,
		|ДокументСерия,
		|ДокументНомер,
		|ДокументДатаВыдачи,
		|ДокументКемВыдан,
		|Страна,
		|ДатаДоговораКонтракта,
		|НомерДоговораКонтракта,
		|ДатаПриема,
		|НомерЛицевогоСчета,
		|ЗарплатныйПроект";
	
	Возврат КадровыеДанные;
	
КонецФункции

