﻿

&НаКлиенте
Процедура ЗагрузитьПравила(Команда)
	
	ЗагрузитьНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьНаСервере()
	
	//FIXME: Ошибка синтаксического контроля
	Узел = Элементы.Список.ТекущаяСтрока.Ссылка;
	Отказ = Ложь;
	СтруктураЗаписи = Новый Структура;
	СтруктураЗаписи.Вставить("ИмяПланаОбмена", "нвОбменВещевоеДовольствие");
	СтруктураЗаписи.Вставить("ВидПравил", Перечисления.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов);
	СтруктураЗаписи.Вставить("ИмяМакетаПравил", "ПравилаОбмена");
	СтруктураЗаписи.Вставить("ИсточникПравил", Перечисления.ИсточникиПравилДляОбменаДанными.МакетКонфигурации);
	СтруктураЗаписи.Вставить("ИспользоватьФильтрВыборочнойРегистрацииОбъектов", Ложь);
	// получаем набор записей регистра
	НаборЗаписей = СоздатьНаборЗаписейРегистраСведений(СтруктураЗаписи, "ПравилаДляОбменаДанными");
	// добавляем только одну запись в новый набор записей
	НоваяЗапись = НаборЗаписей.Добавить();
	// заполняем значения свойств записи из структуры
	ЗаполнитьЗначенияСвойств(НоваяЗапись, СтруктураЗаписи);
	// Загружаем правила для обмена данными в ИБ
	РегистрыСведений.ПравилаДляОбменаДанными.ЗагрузитьПравила(Отказ, НаборЗаписей[0]);
	Если Не Отказ Тогда
		НаборЗаписей.Записать();
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
// Создает набор записей регистра сведений по переданным значениям структуры. Добавляет одну запись в набор.
//
// Параметры:
//  СтруктураЗаписи - Структура - структура по значениям которой необходимо создать набор записей и заполнить этот
//                                набор.
//  ИмяРегистра     - Строка - имя регистра сведений.
// 
Функция СоздатьНаборЗаписейРегистраСведений(СтруктураЗаписи, ИмяРегистра)
	
	МетаданныеРегистра = Метаданные.РегистрыСведений[ИмяРегистра];
	
	// Создаем набор записей регистра.
	НаборЗаписей = РегистрыСведений[ИмяРегистра].СоздатьНаборЗаписей();
	
	// Устанавливаем отбор по измерениям регистра.
	Для Каждого Измерение Из МетаданныеРегистра.Измерения Цикл
		
		// Если задано значение в структуре, то отбор устанавливаем.
		Если СтруктураЗаписи.Свойство(Измерение.Имя) Тогда
			
			НаборЗаписей.Отбор[Измерение.Имя].Установить(СтруктураЗаписи[Измерение.Имя]);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат НаборЗаписей;
	
КонецФункции
