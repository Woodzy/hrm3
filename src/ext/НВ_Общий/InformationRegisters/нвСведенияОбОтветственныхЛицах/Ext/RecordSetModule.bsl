﻿
Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;	
	МассивНепроверяемыхРеквизитов.Добавить("ФизическоеЛицо");
	
	Для Каждого Запись Из ЭтотОбъект Цикл
		Если Не ЗначениеЗаполнено(Запись.ФизическоеЛицо) Тогда
				
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			НСтр("ru = 'Поле ""Сотрудник"" не заполнено'"),
				ЭтотОбъект,
				"ФизическоеЛицо",
				,
				Отказ);
				
		КонецЕсли;
			
		Если НЕ Запись.ПравоПодписиПоДоверенности Тогда
			МассивНепроверяемыхРеквизитов.Добавить("ОснованиеПраваПодписи");
		КонецЕсли;
	КонецЦикла;
		
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(
		ПроверяемыеРеквизиты,
		МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Не Отказ Тогда
		Для Каждого Запись Из ЭтотОбъект Цикл
			Если НЕ Запись.ПравоПодписиПоДоверенности И ЗначениеЗаполнено(Запись.ОснованиеПраваПодписи) Тогда
				Запись.ОснованиеПраваПодписи = "";
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры
