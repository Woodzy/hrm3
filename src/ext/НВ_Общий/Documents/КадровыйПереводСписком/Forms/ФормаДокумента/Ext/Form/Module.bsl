﻿
&НаСервере
Процедура нвПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Пустая() Тогда
		нвОбновитьПодписиНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура нвСотрудникиОбработкаВыбораНаСервере()
	
	Если Объект.Сотрудники.Количество() = 1 Тогда
		нвОбновитьПодписиНаСервере(, Истина);
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура нвСотрудникиПослеУдаленияПосле(Элемент)
	
	нвОбновитьПодписиНаСервере(, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура нвСотрудникиОбработкаВыбораПосле(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	нвСотрудникиОбработкаВыбораНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура нвОбработкаОповещенияПосле(ИмяСобытия, Параметр, Источник)
	
	Если Элементы.Сотрудники.ТекущаяСтрока <> Неопределено И
		Элементы.Сотрудники.ТекущаяСтрока = 0 И ИмяСобытия = "ОбновитьПодписи" Тогда
			нвОбновитьПодписиНаСервере(Параметр, Истина);
	КонецЕсли;	
	
КонецПроцедуры

&НаСервере
Процедура нвОбновитьПодписиНаСервере(Подразделение = Неопределено, ЗаполнятьЕслиНеНайдено = Ложь)

	Если Подразделение = Неопределено Тогда
		Если Объект.Сотрудники.Количество() > 0 Тогда
			Подразделение = Объект.Сотрудники[0].Подразделение;
		КонецЕсли;	
	КонецЕсли;
	
	Если Подразделение = Неопределено Тогда
		Возврат;	
	КонецЕсли;
	
	нвРаботаСоСправочниками.ПодразделениеПриИзмененииНаСервере(Объект, Подразделение, ЗаполнятьЕслиНеНайдено);
	УправлениеСвойствами.ЗаполнитьДополнительныеРеквизитыВФорме(ЭтаФорма, Объект, , ЭтаФорма.Свойства_СкрытьУдаленные);
	

КонецПроцедуры
