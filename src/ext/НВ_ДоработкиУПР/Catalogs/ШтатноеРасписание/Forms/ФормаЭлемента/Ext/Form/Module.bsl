﻿
&НаКлиенте
Процедура нвОрганизацииФИННажатиеПосле(Элемент)
	
	ОткрытьФорму("Справочник.ШтатноеРасписание.Форма.нвуФормаСоответствияОрганизаций", Новый Структура("Организация", ОрганизацияФормы),,,,, 
		Новый ОписаниеОповещения("нвОрганизацииФИННажатиеЗавершение", ЭтотОбъект));
	
КонецПроцедуры

&НаКлиенте
Процедура нвОрганизацииФИННажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Количество() > 0 Тогда
		ОбработатьСоответствия(Результат, ОрганизацияФормы);	
	КонецЕсли;
	нвУстановитьЗаголовкиСсылок(, Ложь, Ложь);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОчиститьРегистр(ИмяРегистра, СоответствиеИзмерений)
	
	НаборЗаписей = РегистрыСведений[ИмяРегистра].СоздатьНаборЗаписей();
	
	Для Каждого Измерение Из СоответствиеИзмерений Цикл
		НаборЗаписей.Отбор[Измерение.Ключ].Установить(Измерение.Значение);
	КонецЦикла;
	
	НаборЗаписей.Записать(Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВопросаОргФИНЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда	
		СоответствиеИзмерений = Новый Соответствие;
		СоответствиеИзмерений.Вставить("ОрганизацияУпр", ОрганизацияФормы);
		ОчиститьРегистр("нвуСоответствиеОрганизаций", СоответствиеИзмерений);
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОбработатьСоответствия(ТаблицаОрг, ОрганизацияУПР)
	
	Для Каждого СтрОрг Из ТаблицаОрг Цикл
		
		НаборЗаписей = РегистрыСведений.нвуСоответствиеОрганизаций.СоздатьНаборЗаписей();
		
		НаборЗаписей.Отбор.ОрганизацияУпр.Установить(ОрганизацияУПР);  
		НаборЗаписей.Отбор.ОрганизацияФин.Установить(СтрОрг.ОрганизацияФИН);
		
		Если СтрОрг.Отметка Тогда	
			НовЗапись = НаборЗаписей.Добавить();

			НовЗапись.ОрганизацияУпр = ОрганизацияУПР;
			НовЗапись.ОрганизацияФин = СтрОрг.ОрганизацияФИН;
		КонецЕсли;
		
		НаборЗаписей.Записать(Истина);
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура нвПодразделенияФИННажатиеПосле(Элемент)
	
	Если Не ЗначениеЗаполнено(Объект.Подразделение) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Не указано подразделение УПР!", , "Подразделение", "Объект.Подразделение");
		Возврат;	
	КонецЕсли;
	
	ОткрытьФорму("Справочник.ШтатноеРасписание.Форма.нвуФормаСоответствияПодразделений", Новый Структура("ОрганизацияУПР, ПодразделениеУПР", ОрганизацияФормы, Объект.Подразделение),,,,, 
		Новый ОписаниеОповещения("нвПодразделенияФИННажатиеЗавершение", ЭтотОбъект));
	
КонецПроцедуры

&НаКлиенте
Процедура нвПодразделенияФИННажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Количество() = 0 Тогда	
		Сообщить("Не установлено ни одного соответствия подразделений УПР - ФИН!");	
	Иначе
		ОбработатьСоответствияПодразделений(Результат, ОрганизацияФормы, Объект.Подразделение);	
	КонецЕсли;
	
	нвУстановитьЗаголовкиСсылок(Ложь, Истина, Ложь);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОбработатьСоответствияПодразделений(ТаблицаФИН, ОрганизацияУПР, ПодразделениеУПР)
	
	Для Каждого Стр Из ТаблицаФИН Цикл
		
		НаборЗаписей = РегистрыСведений.нвуСоответствиеПодразделений.СоздатьНаборЗаписей();
		
		НаборЗаписей.Отбор.ПодразделениеУпр.Установить(ПодразделениеУПР);  
		НаборЗаписей.Отбор.ОрганизацияФин.Установить(Стр.ОрганизацияФИН);
		
		Если ЗначениеЗаполнено(Стр.ПодразделениеФин) Тогда	
			НовЗапись = НаборЗаписей.Добавить();

			НовЗапись.ПодразделениеУпр = ПодразделениеУПР;
			НовЗапись.ОрганизацияФин = Стр.ОрганизацияФИН;
			
			НовЗапись.ПодразделениеФин = Стр.ПодразделениеФин;
		КонецЕсли;
		
		НаборЗаписей.Записать(Истина);
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура нвСтавкиГрафикФИННажатиеПосле(Элемент)
	
	Если Не ЗначениеЗаполнено(Объект.Подразделение) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Не указано подразделение УПР!", , "Подразделение", "Объект.Подразделение");
		Возврат;	
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Должность) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Не указана должность!", , "Должность", "Объект.Должность");
		Возврат;	
	КонецЕсли;
	
	нвПрочитатьСоответствиеШР();
	
	ОткрытьФорму("Справочник.ШтатноеРасписание.Форма.нвуФормаСоответствияШР", Новый Структура("ОрганизацияУПР, ПодразделениеУПР, Должность, СоответствияПозицийШР", ОрганизацияФормы, Объект.Подразделение, Объект.Должность, СоответствияПозицийШР),,,,, 
		Новый ОписаниеОповещения("нвСоответствиеШРНажатиеЗавершение", ЭтотОбъект));
	
КонецПроцедуры
	
&НаКлиенте
Процедура нвСоответствиеШРНажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Количество() = 0 Тогда	
		Сообщить("Не установлено ни одного соответствия позиций ШР!");	
		нвУстановитьЗаголовкиСсылок(Ложь, Ложь);
	Иначе
		ЭтаФорма.Модифицированность = Истина;
		нвСоответствиеШРНажатиеЗавершениеСервер(Результат);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура нвСоответствиеШРНажатиеЗавершениеСервер(Результат)
	
	ТЗ = РеквизитФормыВЗначение("СоответствияПозицийШР");
	ТЗ.Очистить();
	
	Для Каждого Стр Из Результат Цикл
		НоваяСтр = ТЗ.Добавить();
		ЗаполнитьЗначениясвойств(НоваяСтр, Стр);
	КонецЦикла;
	
	ЗначениеВРеквизитФормы(ТЗ, "СоответствияПозицийШР");
	
	нвУстановитьЗаголовкиСсылок(Ложь, Ложь);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОбработатьСоответствияПозицийШР(ТаблицаСоответствий, ПодразделениеУПР, Должность)
	
	ТекДата = НачалоДня(ТекущаяДата());
	
	Для Каждого Стр Из ТаблицаСоответствий Цикл
		НаборЗаписей = РегистрыСведений.нвуСоответствиеПозицийШР.СоздатьНаборЗаписей();
		
		НаборЗаписей.Отбор.ПодразделениеУпр.Установить(ПодразделениеУПР);  
		НаборЗаписей.Отбор.Должность.Установить(Должность);
		НаборЗаписей.Отбор.ПодразделениеФин.Установить(Стр.ПодразделениеФИН);
		НаборЗаписей.Отбор.Период.Установить(ТекДата);
		
		Если ЗначениеЗаполнено(Стр.КоличествоСтавок) ИЛИ ЗначениеЗаполнено(Стр.ГрафикРаботы) Тогда	
			НовЗапись = НаборЗаписей.Добавить();
			
			НовЗапись.ПодразделениеУпр = ПодразделениеУПР;
			НовЗапись.Должность = Должность;
			НовЗапись.ПодразделениеФин = Стр.ПодразделениеФИН;
			НовЗапись.Период = ТекДата;

			НовЗапись.КоличествоСтавок = Стр.КоличествоСтавок;
			НовЗапись.ГрафикРаботы = Стр.ГрафикРаботы;
		КонецЕсли;
		
		Если НаборЗаписей.Количество() > 0 Тогда
			НаборЗаписей.Записать(Истина);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура нвУстановитьВидимостьУпрФин()
	
	ТипИБ = нвуСтруктураКомпанииПовтИсп.ТипУчетаОрганизации(Объект.Владелец);
	ЭтоУпр = (ТипИБ = "УПР" ИЛИ ТипИБ = "");
	
	Элементы.ГруппаФИН.Видимость = ЭтоУпр;
	Элементы.СтавкиГрафикФИН.Видимость = ЭтоУпр;
	Элементы.УправленческийУчетГруппа.Видимость = (Элементы.УправленческийУчетГруппа.Видимость И ЭтоУпр);
	
КонецПроцедуры

&НаСервере
Процедура нвУстановитьЗаголовкиСсылок(ОбновлятьОрг = Истина, ОбновлятьПодр = Истина, ОбновлятьПоз = Истина)
	
	КолСоответствия = нвПолучитьСоответствия(ОрганизацияФормы, Объект.Подразделение, Объект.Должность, 
		ДанныеФормыВЗначение(СоответствияПозицийШР, Тип("ТаблицаЗначений")), ОбновлятьОрг, ОбновлятьПодр, ОбновлятьПоз);
	
	Если ОбновлятьОрг Тогда
		Элементы.ОрганизацииФИН.Заголовок =ПолучитьЗаголовок(КолСоответствия.КолвоОрг, "организации", "Организации");
	КонецЕсли;
	
	Если ОбновлятьПодр Тогда
		Элементы.ПодразделенияФИН.Заголовок = ПолучитьЗаголовок(КолСоответствия.КолвоПодр, "подразделения", "Подразделения");
	КонецЕсли;
	
	Если ОбновлятьПоз Тогда
		Элементы.СтавкиГрафикФИН.Заголовок = ПолучитьЗаголовок(КолСоответствия.КолвоПоз, "графики работы и количество ставок", "Графики работы и количество ставок");	
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьЗаголовок(Колво, Текст1, Текст2)
	
	Возврат ?(Колво = 0, "Настроить " + Текст1 + " ФИН", Текст2 + " ФИН (" + Колво + ")");
	
КонецФункции

&НаСервереБезКонтекста
Функция нвПолучитьСоответствия(Организация, Подразделение, Должность,СоответствияПозицийШР, ОбновлятьОрг, ОбновлятьПодр, ОбновлятьПоз)
		
	Запрос = Новый Запрос; //TODO: отформатировать текст, избавиться от программной сборки. Конструктор должен работать!
	Запрос.Текст = "ВЫБРАТЬ
	               |	СоответствияПозицийШР.ПодразделениеФИН КАК ПодразделениеФИН,
	               |	СоответствияПозицийШР.КоличествоСтавок КАК КоличествоСтавок,
	               |	СоответствияПозицийШР.ГрафикРаботы КАК ГрафикРаботы,
	               |	СоответствияПозицийШР.Должность КАК Должность
	               |ПОМЕСТИТЬ ВТ_СоответствияПозиций
	               |ИЗ
	               |	&СоответствияПозицийШР КАК СоответствияПозицийШР
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |" + ?(ОбновлятьОрг, "
				   |ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	ПодразделенияОрганизаций.Ссылка КАК ПодразделениеУпр,
	               |	ВЫБОР
	               |		КОГДА нвуСоответствиеОрганизаций.ОрганизацияФин ЕСТЬ NULL
	               |			ТОГДА 0
	               |		ИНАЧЕ 1
	               |	КОНЕЦ КАК КолвоОрг,
	               |	0 КАК КолвоПодр,
	               |	0 КАК КолвоПоз
	               |ПОМЕСТИТЬ ВТ_Данные
	               |ИЗ
	               |	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.нвуСоответствиеОрганизаций КАК нвуСоответствиеОрганизаций
	               |		ПО ПодразделенияОрганизаций.Владелец = нвуСоответствиеОрганизаций.ОрганизацияУпр
	               |			И (нвуСоответствиеОрганизаций.ОрганизацияФин <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка))
	               |ГДЕ
	               |	ПодразделенияОрганизаций.Ссылка = &ПодразделениеУпр", "") + "
	               |
	               |" + ?(ОбновлятьОрг И ОбновлятьПодр, "ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ", ?(ОбновлятьПодр, "ВЫБРАТЬ РАЗРЕШЕННЫЕ", "")) + ?(ОбновлятьПодр, "
	               |	ПодразделенияОрганизаций.Ссылка КАК ПодразделениеУпр,
	               |	0 КАК КолвоОрг,
	               |	ВЫБОР
	               |		КОГДА нвуСоответствиеПодразделений.ПодразделениеФин ЕСТЬ NULL
	               |			ТОГДА 0
	               |		ИНАЧЕ 1
	               |	КОНЕЦ КАК КолвоПодр,
	               |	0 КАК КолвоПоз
				   |", "") + ?(НЕ ОбновлятьОрг И ОбновлятьПодр, " ПОМЕСТИТЬ ВТ_Данные ", "") + ?(ОбновлятьПодр, "
	               |ИЗ
	               |	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.нвуСоответствиеПодразделений КАК нвуСоответствиеПодразделений
	               |		ПО ПодразделенияОрганизаций.Ссылка = нвуСоответствиеПодразделений.ПодразделениеУпр
	               |			И (нвуСоответствиеПодразделений.ПодразделениеФин <> ЗНАЧЕНИЕ(Справочник.ПодразделенияОрганизаций.ПустаяСсылка))
	               |ГДЕ
	               |	ПодразделенияОрганизаций.Ссылка = &ПодразделениеУпр", "") + "
	               |
	               |" + ?((ОбновлятьОрг ИЛИ ОбновлятьПодр) И ОбновлятьПоз, "ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ", ?(ОбновлятьПоз, "ВЫБРАТЬ РАЗРЕШЕННЫЕ", "")) + ?(ОбновлятьПоз, "
	               |	ПодразделенияОрганизаций.Ссылка КАК ПодразделениеУпр,
	               |	0 КАК КолвоОрг,
	               |	0 КАК КолвоПодр,
	               |	ВЫБОР
	               |		КОГДА ВТ_СоответствияПозиций.ПодразделениеФИН ЕСТЬ NULL
	               |			ТОГДА 0
	               |		ИНАЧЕ 1
	               |	КОНЕЦ КАК КолвоПоз
				   |", "") + ?(НЕ ОбновлятьОрг И НЕ ОбновлятьПодр, " ПОМЕСТИТЬ ВТ_Данные ", "") + ?(ОбновлятьПоз, "
	               |ИЗ
	               |	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	               |		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_СоответствияПозиций КАК ВТ_СоответствияПозиций
	               |		ПО (ВТ_СоответствияПозиций.КоличествоСтавок > 0
	               |				ИЛИ ВТ_СоответствияПозиций.ГрафикРаботы <> ЗНАЧЕНИЕ(Справочник.ГрафикиРаботыСотрудников.ПустаяСсылка))
	               |			И (ВТ_СоответствияПозиций.Должность = &Должность)
	               |ГДЕ
	               |	ПодразделенияОрганизаций.Ссылка = &ПодразделениеУпр", "") + "
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	нвуСоответствиеПодразделений.ПодразделениеФин КАК ПодразделениеФин,
	               |	нвуСоответствиеПодразделений.ПодразделениеУпр КАК ПодразделениеУпр
	               |ИЗ
	               |	РегистрСведений.нвуСоответствиеПодразделений КАК нвуСоответствиеПодразделений
	               |ГДЕ
	               |	нвуСоответствиеПодразделений.ПодразделениеУпр = &ПодразделениеУпр
	               |	И нвуСоответствиеПодразделений.ПодразделениеФин <> ЗНАЧЕНИЕ(Справочник.ПодразделенияОрганизаций.ПустаяСсылка)
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВТ_СоответствияПозиций.ПодразделениеФИН КАК ПодразделениеФИН,
	               |	ВТ_СоответствияПозиций.Должность КАК Должность
	               |ИЗ
	               |	ВТ_СоответствияПозиций КАК ВТ_СоответствияПозиций
	               |ГДЕ
	               |	(ВТ_СоответствияПозиций.КоличествоСтавок > 0
	               |			ИЛИ ВТ_СоответствияПозиций.ГрафикРаботы <> ЗНАЧЕНИЕ(Справочник.ГрафикиРаботыСотрудников.ПустаяСсылка))
	               |	И ВТ_СоответствияПозиций.Должность = &Должность
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВТ_Данные.ПодразделениеУпр КАК ПодразделениеУпр,
	               |	СУММА(ВТ_Данные.КолвоОрг) КАК КолвоОрг,
	               |	СУММА(ВТ_Данные.КолвоПодр) КАК КолвоПодр,
	               |	СУММА(ВТ_Данные.КолвоПоз) КАК КолвоПоз
	               |ИЗ
	               |	ВТ_Данные КАК ВТ_Данные
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	ВТ_Данные.ПодразделениеУпр";
	
	Запрос.УстановитьПараметр("ОрганизацияУпр", ?(ЗначениеЗаполнено(Организация), Организация, Подразделение.Владелец));
	Запрос.УстановитьПараметр("ПодразделениеУпр", Подразделение);
	Запрос.УстановитьПараметр("СоответствияПозицийШР", СоответствияПозицийШР);
	Запрос.УстановитьПараметр("Должность", Должность);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	СтруктураКолво = Новый Структура("КолвоОрг, КолвоПодр, КолвоПоз", 0, 0, 0);
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(СтруктураКолво, Выборка);
	КонецЕсли;
	
	Возврат СтруктураКолво;
	
КонецФункции

&НаКлиенте
Процедура нвПриОткрытииПосле(Отказ)
	
	нвПрочитатьСоответствиеШР();
	нвУстановитьВидимостьУпрФин();
	нвУстановитьЗаголовкиСсылок();
	
КонецПроцедуры

&НаКлиенте
Процедура нвВладелецПриИзмененииПосле(Элемент)
	
	нвУстановитьВидимостьУпрФин();
	
КонецПроцедуры

&НаСервере
Процедура нвПрочитатьСоответствиеШР(этоИзменениеШР = Ложь)
	
	ТЗ = РеквизитФормыВЗначение("СоответствияПозицийШР");
	//сохраняем внесенные вручную изменения
	ТЗТекущая = ТЗ.Скопировать();
	ТЗ.Очистить();
	
	Если ЗначениеЗаполнено(Объект.Подразделение) И ЗначениеЗаполнено(Объект.Должность) Тогда
		ОрганизацияУПР = ?(ЗначениеЗаполнено(ОрганизацияФормы), ОрганизацияФормы, Объект.Подразделение.Владелец);
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
		|	СоответствиеОрганизаций.ОрганизацияФин КАК ОрганизацияФИН,
		|	СоответствиеОрганизаций.ОрганизацияФин.Наименование КАК ОрганизацияФинНаименование,
		|	ЕСТЬNULL(нвуСоответствиеПодразделений.ПодразделениеФин, ЗНАЧЕНИЕ(Справочник.ПодразделенияОрганизаций.ПустаяСсылка)) КАК ПодразделениеФин
		|ПОМЕСТИТЬ ВТ_Подразделения
		|ИЗ
		|	РегистрСведений.нвуСоответствиеОрганизаций КАК СоответствиеОрганизаций
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.нвуСоответствиеПодразделений КАК нвуСоответствиеПодразделений
		|		ПО СоответствиеОрганизаций.ОрганизацияУпр = нвуСоответствиеПодразделений.ПодразделениеУпр.Владелец
		|			И СоответствиеОрганизаций.ОрганизацияФин = нвуСоответствиеПодразделений.ОрганизацияФин
		|			И (нвуСоответствиеПодразделений.ПодразделениеУпр = &ПодразделениеУПР)
		|ГДЕ
		|	СоответствиеОрганизаций.ОрганизацияУпр = &ОрганизацияУпр
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	нвуСоответствиеПозицийШР.Период КАК Период,
		|	нвуСоответствиеПозицийШР.ПодразделениеУпр КАК ПодразделениеУпр,
		|	нвуСоответствиеПозицийШР.Должность КАК Должность,
		|	нвуСоответствиеПозицийШР.ПодразделениеФин КАК ПодразделениеФин,
		|	нвуСоответствиеПозицийШР.КоличествоСтавок КАК КоличествоСтавок,
		|	нвуСоответствиеПозицийШР.ГрафикРаботы КАК ГрафикРаботы
		|ПОМЕСТИТЬ ВТ_СоответствияШР
		|ИЗ
		|	РегистрСведений.нвуСоответствиеПозицийШР КАК нвуСоответствиеПозицийШР
		|ГДЕ
		|	нвуСоответствиеПозицийШР.ПодразделениеУпр = &ПодразделениеУпр
		|	И нвуСоответствиеПозицийШР.Должность = &Должность
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	МАКСИМУМ(ВТ_СоответствияШР.Период) КАК Период
		|ПОМЕСТИТЬ ВТ_МаксПериодСоответствия
		|ИЗ
		|	ВТ_СоответствияШР КАК ВТ_СоответствияШР
		|ГДЕ
		|	ВТ_СоответствияШР.Период <= &ТекДата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_Подразделения.ОрганизацияФИН КАК ОрганизацияФИН,
		|	ВТ_Подразделения.ОрганизацияФинНаименование КАК ОрганизацияФинНаименование,
		|	ВТ_Подразделения.ПодразделениеФин КАК ПодразделениеФин,
		|	&Должность КАК Должность,
		|	ЕСТЬNULL(ВТ_СоответствияШР.КоличествоСтавок, 0) КАК КоличествоСтавок,
		|	ЕСТЬNULL(ВТ_СоответствияШР.ГрафикРаботы, ЗНАЧЕНИЕ(Справочник.ГрафикиРаботыСотрудников.ПустаяСсылка)) КАК ГрафикРаботы
		|ИЗ
		|	ВТ_Подразделения КАК ВТ_Подразделения
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_МаксПериодСоответствия КАК ВТ_МаксПериодСоответствия
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_СоответствияШР КАК ВТ_СоответствияШР
		|			ПО ВТ_МаксПериодСоответствия.Период = ВТ_СоответствияШР.Период
		|		ПО (ВТ_Подразделения.ПодразделениеФин = ВТ_СоответствияШР.ПодразделениеФин)";
		
		Запрос.УстановитьПараметр("ОрганизацияУпр", ОрганизацияУПР);
		Запрос.УстановитьПараметр("ПодразделениеУпр", Объект.Подразделение);
		Запрос.УстановитьПараметр("Должность", Объект.Должность);
		Запрос.УстановитьПараметр("ТекДата", НачалоДня(ТекущаяДата()));
		
		ТЗ = Запрос.Выполнить().Выгрузить();
		
		Если Не этоИзменениеШР Тогда
			Для Каждого Стр Из ТЗТекущая Цикл
				СтрокиПоПодр = ТЗ.НайтиСтроки(Новый Структура("ПодразделениеФин, Должность", Стр.ПодразделениеФин, Стр.Должность));
				Если СтрокиПоПодр.Количество() > 0 Тогда
					ЗаполнитьЗначенияСвойств(СтрокиПоПодр[0], Стр, "КоличествоСтавок, ГрафикРаботы");	
				КонецЕсли;
			КонецЦикла;
		КонецЕсли;
		
	КонецЕсли;
	
	ЗначениеВРеквизитФормы(ТЗ, "СоответствияПозицийШР");
	
КонецПроцедуры

&НаСервере
Процедура нвПослеЗаписиНаСервереПосле(ТекущийОбъект, ПараметрыЗаписи)
	
	 нвОбновитьДанныеФИН(ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура нвОбновитьДанныеФИН(ШЕУпр)
	
	Если Не ЗначениеЗаполнено(ШЕУпр.Ссылка) Тогда
		Возврат;	
	КонецЕсли;
	
	ОбработатьСоответствияПозицийШР(ДанныеФормыВЗначение(СоответствияПозицийШР, Тип("ТаблицаЗначений")), ШЕУпр.Подразделение, ШЕУпр.Должность);
	
	Если нвуСтруктураКомпанииПовтИсп.ТипУчетаОрганизации(ОрганизацияФормы) <> "УПР" Тогда
		Возврат;	
	КонецЕсли;
	  
	нвуШтатноеРасписание.нвуПроверитьСоздатьЗаписьФИН(ШЕУпр.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура нвуОбработкаОповещенияПосле(ИмяСобытия, Параметр, Источник)
	
	нвуОбработкаОповещенияПослеНаСерввере(ИмяСобытия);
	
КонецПроцедуры

&НаСервере
Процедура нвуОбработкаОповещенияПослеНаСерввере(ИмяСобытия)

	Если НЕ ВнешниеДанные Тогда
		Если ИмяСобытия = "СохраненыПозицииШтатногоРасписания" Тогда
			нвПрочитатьСоответствиеШР(Истина);
			нвУстановитьЗаголовкиСсылок();	
		КонецЕсли;
	Иначе
		Если ИмяСобытия = "ИзмененыДанныеПозицииШтатногоРасписания" Тогда
			ШЕОбъект = РеквизитФормыВЗначение("Объект");
			нвОбновитьДанныеФИН(ШЕОбъект);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры
