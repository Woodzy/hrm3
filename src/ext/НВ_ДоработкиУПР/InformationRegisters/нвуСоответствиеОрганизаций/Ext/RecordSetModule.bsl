﻿
Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого СтрокаНабора Из ЭтотОбъект Цикл
		
		Если СтрокаНабора.ОрганизацияУпр = СтрокаНабора.ОрганизацияФин Тогда
			ОбменДаннымиСервер.СообщитьОбОшибке("Нельзя указывать одну организцию как УПР и ФИН одновременно",Отказ);
		КонецЕсли;
		
		ТипУчетаУпр = нвуСтруктураКомпанииПовтИсп.ТипУчетаОрганизации(СтрокаНабора.ОрганизацияУпр);
		ТипУчетаФин = нвуСтруктураКомпанииПовтИсп.ТипУчетаОрганизации(СтрокаНабора.ОрганизацияФин);
		
		Если ТипУчетаУпр = "ошибка" Тогда
			ОбменДаннымиСервер.СообщитьОбОшибке("Организация """+СтрокаНабора.ОрганизацияУпр+""" настроена как УПР и ФИН одновременно. Исправьте текущие настройки по организации",Отказ);
		КонецЕсли;
		Если ТипУчетаУпр = "ФИН" Тогда
			ОбменДаннымиСервер.СообщитьОбОшибке("Организация """+СтрокаНабора.ОрганизацияУпр+""" уже настроена как ФИН. Нельзя указывать ее как УПР",Отказ);
		КонецЕсли;
		
		Если ТипУчетаФин = "ошибка" Тогда
			ОбменДаннымиСервер.СообщитьОбОшибке("Организация """+СтрокаНабора.ОрганизацияФин+""" настроена как УПР и ФИН одновременно. Исправьте текущие настройки по организации",Отказ);
		КонецЕсли;
		Если ТипУчетаФин = "УПР" Тогда
			ОбменДаннымиСервер.СообщитьОбОшибке("Организация """+СтрокаНабора.ОрганизацияФин+""" уже настроена как УПР. Нельзя указывать ее как ФИН",Отказ);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры
